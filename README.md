```
$ sudo apt install firewalld
$ sudo firewall-cmd --permanent --add-port=8080/tcp
$ sudo firewall-cmd --permanent --add-port=50000/tcp
$ sudo firewall-cmd --reload
```


```
$ sudo iptables -t nat -N DOCKER
$ sudo iptables -N DOCKER
$ sudo systemctl restart docker
```


```
# cat docker-compose.yml

version: "3.3"
services:
  jenkins:
    container_name: jenkins
    image: jenkins/jenkins:lts
    privileged: true
    user: root
    ports:
    - 8080:8080
    - 50000:50000
    volumes:
    - ./jenkins_home/:/var/jenkins_home/:rw
    - /var/run/docker.sock:/var/run/docker.sock
```


```
$ sudo docker-compose pull
$ sudo docker-compose up -d

$ sudo docker-compose ps
```

